package com.tecsq.Helper;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tecsq.filechecker.FileChecker;

/**
 * Created by NBIT02 on 2/10/2017.
 */

public class Constant {
    private final static FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
    public final static DatabaseReference mFirebaseDatabase = mFirebaseInstance.getReference("licenses");
}
