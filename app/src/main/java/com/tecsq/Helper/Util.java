package com.tecsq.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.DateFormat;
import android.view.WindowManager;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.tecsq.geointel.Activation_Act;
import com.tecsq.geointel.Geo_Main;
import com.tecsq.geointel.R;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;


public class Util {

    private static AmazonS3Client sS3Client;
    private static TransferUtility sTransferUtility;
    public static ProgressDialog ringProgressDialog = null;
    public static String deviceCode;

    public static AmazonS3Client getS3Client(Context context) {
    	AWSCredentials credentials = new BasicAWSCredentials("AKIAI4CHGTRARIBDTHEA","Xw6Ip1li5LzTCf70SrRQQ1wT02u2nEZgs7hlYlKR");
        if (sS3Client == null) {
            sS3Client = new AmazonS3Client(credentials, new ClientConfiguration()
            .withSocketTimeout(0)
            .withProtocol(Protocol.HTTP));
            sS3Client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_CENTRAL_1 ));
        }
        return sS3Client;
    }


    public static TransferUtility getTransferUtility(Context context) {
        if (sTransferUtility == null) {
            sTransferUtility = new TransferUtility(getS3Client(context.getApplicationContext()),
                    context.getApplicationContext());
        }
        return sTransferUtility;
    }

    public static String getBytesString(long bytes) {
        String[] quantifiers = new String[] {
                "KB", "MB", "GB", "TB"
        };
        double speedNum = bytes;
        for (int i = 0;; i++) {
            if (i >= quantifiers.length) {
                return "";
            }
            speedNum /= 1024;
            if (speedNum < 512) {
                return String.format("%.2f", speedNum) + " " + quantifiers[i];
            }
        }
    }

    public static void launchProgressDialog(Context ctx, String title, String message, OnLaunchProgressDialog execute) {
        if (ringProgressDialog == null || !ringProgressDialog.isShowing()) {
            ringProgressDialog = ProgressDialog.show(ctx, title, message, true);
            ringProgressDialog.setCancelable(false);
            ringProgressDialog.setCanceledOnTouchOutside(false);
            execute.onLaunch();
        }
    }

    public String getMacCode(Context context) {
        try{
            WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            String macaddress = info.getMacAddress();
            return macaddress;
        }catch(Exception e) {
            //ErrorAlert("Error: " + e, false);
        }
        return  null;
    }

    public void saveActivationCode(Context context ,String text,String fileName) {
        try {
            File file = new File(context.getExternalFilesDir(null),fileName);
            File geosense = new File(context.getExternalFilesDir(null),"geosense.tecgeo");
            geosense.delete();
            file.createNewFile();
            BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
            buf.append(text);
            buf.close();

        } catch(IOException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String readLicenseCode(Context context,String fileName){
        String sCurrentLine = null;
        try {
            File filename = new File(context.getExternalFilesDir(null),fileName);
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String next = " ";

            while((next = br.readLine()) != null){
                sCurrentLine = next;
            }
            br.close();
            return  sCurrentLine;

        }catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void ErrorAlert(final Context context,String AlertMsg, final Boolean showSettings)
    {
        AlertDialog alert ;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("TSQ-Geo Intel");
        builder.setMessage(AlertMsg);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(showSettings) {
                    Intent refresh = new Intent(context,Geo_Main.class);
                    ((Activity) context).finish();
                    context.startActivity(refresh);
                } else {
                    //ringProgressDialog.dismiss();
                    dialog.dismiss();
                    Intent dialogIntent = new Intent(context, Activation_Act.class);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(dialogIntent);
//                    Intent refresh = new Intent(context,Geo_Main.class);
//                    ((Activity) context).finish();
//                    context.startActivity(refresh);
                }
            }
        });
        alert = builder.create();
        alert.setCancelable(true);
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();
    }

    public String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
        return date;
    }

}
