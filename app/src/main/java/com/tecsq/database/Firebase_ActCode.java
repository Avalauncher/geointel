package com.tecsq.database;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by NBIT02 on 2/7/2017.
 */

@IgnoreExtraProperties
public class Firebase_ActCode {

    public Object id;
    public Object act_code;
    public Object organization;
    public int status;
    public Object device_code;
    public Object project;
    public Object act_date;

    public Firebase_ActCode(){}

    public Firebase_ActCode(Object id,Object act_code,Object organization,int status,Object device_code,Object project,Object act_date){
        this.id = id;
        this.act_code = act_code;
        this.organization = organization;
        this.status = status;
        this.device_code = device_code;
        this.project = project;
        this.act_date = act_date;
    }
}
