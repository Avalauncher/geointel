package com.tecsq.database;

/**
 * Created by NBIT02 on 2/8/2017.
 */

public class Firebase_Licenses {

    public String key;
    public Firebase_ActCode fb_code;

    public Firebase_Licenses(){}

    public Firebase_Licenses(String key,Firebase_ActCode fb_code){
        this.key = key;
        this.fb_code = fb_code;
    }

}
