package com.tecsq.filechecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.format.Time;

public class FileChecker {

    public void CheckCreateFile(Context context)
	{  
    	String sCurrentLine = null;
		try {
			File filename = new File(context.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}catch (IOException e) {
			e.printStackTrace();
		}
    	
		Time now = new Time();
	    now.setToNow();
	    String trimNow = now.toString().substring(0, 8);
		TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		//String logFileName = "geointel_" + telephonyManager.getDeviceId().toString() + "_" + trimNow + "_log" ;
		String logFileName = sCurrentLine + "_" + telephonyManager.getDeviceId().toString() + "_" + trimNow + "_log" ;

		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         e.printStackTrace();
	      }
		}

	}

	 public static String DeviceID(Context context) {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			telephonyManager.getDeviceId();
			String deviceid = telephonyManager.getDeviceId().toString();
			return deviceid;
	 }
	 
	 public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
		    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
		        if (serviceClass.getName().equals(service.service.getClassName())) {
		            return true;
		        }
		    }
		    return false;
		}	 
    
}



