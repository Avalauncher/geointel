package com.tecsq.geointel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tecsq.Helper.Constant;
import com.tecsq.Helper.Util;
import com.tecsq.database.Firebase_ActCode;
import com.tecsq.database.Firebase_Licenses;
import com.tecsq.filechecker.FileChecker;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.StrictMode;
import android.view.WindowManager;


public class Activation_Checker extends Service{
	
//	public final static String URL = "http://www.nmnglobal.com/tsqgeolicense/tsqgeo_activation.php?wsdl";
//	public static final String NAMESPACE = "http://www.nmnglobal.com/";
//	public static final String SOAP_ACTION_PREFIX = "/";
//	public static String methodName = "";
//	Boolean SoapError = false;
//	//String deviceCode = "";
//    //private PendingIntent pendingIntent;
//	String actCode;
	private Context context;
	private Util util = new Util();
	private String[] fileName = {"geosense.tecgeo","tec.act"};
	String myImei;
	private String textActivationCode;
	private AlertDialog alert;


	public Activation_Checker() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
    public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		context = this;
		textActivationCode = util.readLicenseCode(context,fileName[1]);
		myImei = FileChecker.DeviceID(this);
		if (util.isNetworkAvailable(context)) {
			if(FileChecker.isMyServiceRunning(ServiceMain.class, Activation_Checker.this)) {
				//methodName="check_device_code";
				//AsyncTaskRunner runner=new AsyncTaskRunner();
				//runner.execute();
				final Query queryref = Constant.mFirebaseDatabase.orderByChild("act_code").equalTo(textActivationCode);
				if(queryref != null){
					queryref.addValueEventListener(new ValueEventListener() {

						@Override
						public void onDataChange(DataSnapshot dataSnapshot) {
							if(dataSnapshot.getChildren() != null){
								onDataSnapShotChange(dataSnapshot);
							}else{
								System.out.println("License is invalid!");
							}
						}

						@Override
						public void onCancelled(DatabaseError databaseError) {

						}
					});
				}
			}
			
		}

	}

	private void onDataSnapShotChange(DataSnapshot dataSnapshot) {
		for(DataSnapshot getData:dataSnapshot.getChildren()){
			Firebase_ActCode data = getData.getValue(Firebase_ActCode.class);
			Firebase_Licenses license = new Firebase_Licenses(getData.getKey(),data);
			if(license.fb_code.act_code.equals(textActivationCode)){
				switch (license.fb_code.status){
					case 1:
						System.out.println("activation code act checker----- " + license.fb_code.act_code);
						//util.saveActivationCode(context,);
						//util.ErrorAlert(context,"License Already Expired!",false);
						startService(new Intent(Activation_Checker.this, FTPService.class));
						stopSelf();
						break;
					case 2:
						stopSelf();
						System.out.println("activation code act checker ----- " + license.fb_code.act_code);
						textActivationCode = "";
						util.saveActivationCode(context,textActivationCode,fileName[1]);
						ErrorAlert("License Already Expired",true);
						break;
				}
			}
		}
	}

//	private boolean isNetworkAvailable() {
//
//		ConnectivityManager connectivityManager
//	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//
//	}
	
//	private class AsyncTaskRunner extends AsyncTask<String,String,String>{
//		private String err;
//		@Override
//		protected String doInBackground(String... params) {
//			try{
//
//				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//				SoapObject request = new SoapObject(NAMESPACE, methodName);
//				request.addProperty("device_code",myImei);
//
//				envelope.setOutputSoapObject(request);
//				envelope.dotNet = true;
//
//				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//				androidHttpTransport.call(NAMESPACE +  methodName, envelope);
//
//				SoapObject result = (SoapObject)envelope.bodyIn;
//				System.out.println(result);
//				err = result.getProperty(0).toString();
//
//			} catch (SoapFault sf) {
//				sf.printStackTrace();
//				err = sf.getMessage();
//				SoapError=true;
//			} catch (Exception e) {
//				e.printStackTrace();
//				err = e.getMessage();
//				SoapError=true;
//			}
//
//			return err;
//		}
//
//		protected void onPostExecute(String result){
//
//			try {
//				//Log.i("XXXXX", "" + result);
//
//				//ErrorAlert("xcgdgdf", false);
//				if(!SoapError){
//					if(result.equalsIgnoreCase("no")) {
//						ErrorAlert("Please Register Your Device!", true);
//
//					} else {
////				        Intent myIntent = new Intent(Activation_Checker.this, FTPService.class);
////				        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
////				        pendingIntent = PendingIntent.getService(Activation_Checker.this, 445454, myIntent, 0);
////				        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 300000 , pendingIntent);
//						startService(new Intent(Activation_Checker.this, FTPService.class));
//						stopSelf();
//					}
//				} else {
//					stopSelf();
//				}
//
//			} catch(Exception e) {
//				//ErrorAlert("Error Connecting to Activation Portal!", false);
//				stopSelf();
//			}
//
//		}
//
//		@Override
//		protected void onPreExecute() {	}
//
//		@Override
//		protected void onProgressUpdate(String... text) { }
//
//	}//end of runner
	
	private void ErrorAlert(String AlertMsg, final Boolean showSettings){
		
//	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
//	    LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
//	    //LayoutInflater inflater = this.getLayoutInflater();
//	    final View dialogView = inflater.inflate(R.layout.activation_dialog, null);
//	    builder.setView(dialogView);

//	    final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);		
		
        AlertDialog.Builder builder = new AlertDialog.Builder(Activation_Checker.this);
        builder.setTitle("TSQ-Geo Intel");
        builder.setCancelable(true);
        builder.setMessage(AlertMsg);
        builder.setIcon(R.drawable.ic_launcher);
        
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(showSettings) {
					Intent dialogIntent = new Intent(getBaseContext(), Activation_Act.class);
					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplication().startActivity(dialogIntent);
					dialog.dismiss();
//					new Activation(Activation_Checker.this);
//					actCode = edt.getText().toString();
//					methodName="check_activation";
//					AsyncTaskRunnerActivation runnerAct=new AsyncTaskRunnerActivation();
//					runnerAct.execute();
				} 
			}
		});	        
		
	    alert = builder.create();
	    alert.setCancelable(true);
	    alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		alert.show();
    }		
	

}
