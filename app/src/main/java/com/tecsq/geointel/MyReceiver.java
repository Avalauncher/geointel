package com.tecsq.geointel;

import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.format.Time;

public class MyReceiver extends BroadcastReceiver {
	
	public String trimNow;
	Time now = new Time();
	CallLog callerLog = new CallLog();

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		
		now.setToNow();
      	Calendar c = Calendar.getInstance(); 
      	Integer ms =  c.get(Calendar.MILLISECOND);
      	trimNow = now.toString().substring(0, 15) + "." + ms.toString();

		if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
	    	Intent i = new Intent(Intent.ACTION_MAIN);
	    	i.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.Geo_Main"));
	        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(i);
		}

		if (intent.getAction() == "android.net.conn.CONNECTIVITY_CHANGE") {
	     	    	Intent ftpUpload = new Intent(Intent.ACTION_MAIN);
	     	    	ftpUpload.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.FTPService"));
	     	    	ftpUpload.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    	        context.startService(ftpUpload);
	    			//Toast.makeText(context, "FTP Resend",Toast.LENGTH_LONG).show();
		}
		
	    if (Intent.ACTION_NEW_OUTGOING_CALL.equals(intent.getAction())) {
	        final String originalNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
	        System.out.println("outgoing,ringing:"  + originalNumber);
	        callerLog.appendErrorLog(context,  "," + "," + "," + trimNow + "," + "," + "," + "," + "OUTGOINGCALL: " + originalNumber);
	    }
	    
	    try {
	    	
		    if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
	                TelephonyManager.EXTRA_STATE_RINGING)) {
			    final String stringExtra = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
			    if (TelephonyManager.EXTRA_STATE_RINGING.equals(stringExtra)) {
		            final String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
		            System.out.println("incoming,ringing:" + incomingNumber);
		            callerLog.appendErrorLog(context,  "," + "," + "," + trimNow + "," + "," + "," + "," + "INCOMINGCALL:" + incomingNumber);
		        } else if (stringExtra.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
		            final String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
		            System.out.println("incoming,idle:" + incomingNumber);
		            callerLog.appendErrorLog(context,  "," + "," + "," + trimNow + "," + "," + "," + "," + "INCOMINGCALL:" + incomingNumber);
		        } else if (stringExtra.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
		            final String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
		            System.out.println("incoming,offhook:" + incomingNumber);
		            callerLog.appendErrorLog(context,  "," + "," + "," + trimNow + "," + "," + "," + "," + "INCOMINGCALL:" + incomingNumber);		        }
		    }
	    
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	    
	}
	

}