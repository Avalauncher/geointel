package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import com.tecsq.filechecker.FileChecker;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
//import android.os.Environment;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.format.Time;
import android.view.View;

public class Geo_Main extends Activity {
	Time now = new Time();
	String errTime ;
	//ErrorLog errorLog = new ErrorLog(); 
	ftpErrorLog SendError = new ftpErrorLog();
	public static String deviceCode = "";
	private Context context = this;
	private  PendingIntent pending1Minute, pendingStarter;

	//@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_geo_main);
		
//		now = new Time();
//	    now.setToNow();
//	    String trimNow = now.toString().substring(0, 8);

	  	//errTime = now.toString();
		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": GeoMain (onCreate) Started");
		//SendError.ftpSend(Geo_Main.this);

//	    String logFileName = "geointel_" + FileChecker.DeviceID(Geo_Main.this) + "_" + trimNow + "_log" ;
//	    File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
//		if (!logFile.exists())
//		{
//	      try
//	      {
//	    	  logFile.createNewFile();
//	    	  	now.setToNow();
//	    	  	errTime = now.toString();
//	    		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ":New Log File Created");
//	    		//SendError.ftpSend(Geo_Main.this);
//	      } 
//	      catch (IOException e)
//	      {
//    	  	now.setToNow();
//    	  	errTime = now.toString();
//    		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ":File Create Error");
//    		//SendError.ftpSend(Geo_Main.this);
//	         e.printStackTrace();
//	      }
//		}
		
		//activation here
		checkActivation();

	}

	public void startServiceMain(View v) { }

    
	public void checkActivation() {

		String sCurrentLine = null;	
		File filename = new File(getExternalFilesDir(null),"geosense.tecgeo");

		if(filename.exists()) {
			
			try {
			   BufferedReader br = new BufferedReader(new FileReader(filename));
			   String next = " ";	
			   while((next = br.readLine()) != null){
			        sCurrentLine = next;
			   }
	
			   br.close();
		      
			} catch (IOException e) {
				
			}
			
			if(sCurrentLine.equalsIgnoreCase("0")) {
				 //Log.i("sCurrentLine","" + sCurrentLine);
				try {
					//TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					String deviceid = FileChecker.DeviceID(Geo_Main.this);			
					deviceCode = deviceid;
					// Log.i("deviceCode","" + deviceCode);
					new Activation(Geo_Main.this);
				} catch (Exception e) {	
					getMacCode();
				}

			}

		} else {
	        Intent intent = new Intent();
	        intent.setAction("com.example.SendBroadcast");
	        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
	        sendBroadcast(intent);
	        
//	        Intent myIntent1Minute = new Intent(Geo_Main.this, GeoIntelStopper.class);
//			AlarmManager alarmiMinute = (AlarmManager)getSystemService(ALARM_SERVICE);
//	        pending1Minute = PendingIntent.getService(Geo_Main.this, 445454, myIntent1Minute, 0);
//	        alarmiMinute.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000,pending1Minute);
//
//	        Intent myIntentStarter = new Intent(Geo_Main.this, GeoIntelStarter.class);
//			AlarmManager alarmStarter = (AlarmManager)getSystemService(ALARM_SERVICE);
//	        pendingStarter = PendingIntent.getService(Geo_Main.this, 445454, myIntentStarter, 0);
//	        alarmStarter.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000,pendingStarter);	        

			startService(new Intent(this, ServiceMain.class));
			finish();
		}
	}
	
	public void getMacCode() {
		try{
			WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = manager.getConnectionInfo();
			String macaddress = info.getMacAddress();
			deviceCode = macaddress;
			//Log.i("deviceCode","" + deviceCode);
			new Activation(Geo_Main.this);
		}catch(Exception e) {
			//ErrorAlert("Error: " + e, false);		
		}
	}    
    
	
}
