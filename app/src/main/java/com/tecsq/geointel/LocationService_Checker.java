package com.tecsq.geointel;

import com.tecsq.filechecker.FileChecker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.view.View.OnClickListener;

public class LocationService_Checker extends Service implements LocationListener{
	LocationManager locationManager;
	AlertDialog alert;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}

    public LocationService_Checker() {
	}
    
	@Override
    public void onStart(Intent intent, int startId) {
    	locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,0, this);
	
		boolean isGPS = locationManager.isProviderEnabled (LocationManager.GPS_PROVIDER); 
		
		if(FileChecker.isMyServiceRunning(ServiceMain.class, LocationService_Checker.this)) {
			if(!isGPS){

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				
				builder.setTitle("TSQ - Geo Intel");
				
				builder.setCancelable(true);
				
				builder.setMessage("Please Turn On Location Services!");
				
				builder.setIcon(R.drawable.ic_launcher);

				
				builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {

							dialog.dismiss();
							Intent dialogIntent = new Intent(getBaseContext(), GPSSet.class);
							dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							getApplication().startActivity(dialogIntent);


					}
				});
			
			    alert = builder.create();
			    alert.setCancelable(false);
			    alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
				alert.show();
				
				
				
			}
		}
		

		
	}
	
    public void checkGps(){
		final Dialog dialog=new Dialog(this);
    	dialog.setContentView(R.layout.gps);
    	dialog.setTitle("GeoIntel GPS Settings");
    	dialog.setCancelable(false);
    	dialog.show();
    	
    	Button gpsSettings = (Button) dialog.findViewById(R.id.gps_sett_button);
    	gpsSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Intent refresh = new Intent(LocationService_Checker.this, GPSSet.class);
				stopSelf();
				LocationService_Checker.this.startActivity(refresh);	
				//startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
			}
		});
    }

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
    
}
