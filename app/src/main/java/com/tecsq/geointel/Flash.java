package com.tecsq.geointel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;
import com.tecsq.filechecker.FileChecker;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;

public class Flash extends Activity {

	private Timer dialogTimer;
	boolean finishCopy;
	
	public final static String URL = "http://www.nmnglobal.com/tsqgeolicense/tsqgeo_activation.php?wsdl";
	public static final String NAMESPACE = "http://www.nmnglobal.com/";
	public static final String SOAP_ACTION_PREFIX = "/";
	public static String methodName = "";
	Boolean SoapError = false;	
	private Context context = this;
	public static String deviceCode = "";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        
		try {
			String deviceid = FileChecker.DeviceID(Flash.this);			
			deviceCode = deviceid;

		} catch (Exception e) {	
			//getMacCode();
		}
        
        finishCopy = false;
        
		File file = new File(getExternalFilesDir(null),"dummy.tecgeo");
        if (!file.exists()) {
            copyAssets();        	
        } else {
        	finishCopy = true;
//			methodName="check_device_code";
//			AsyncTaskRunner runner=new AsyncTaskRunner();
//			runner.execute();
        }

        if (finishCopy) {
    		dialogTimer = new Timer();
    		dialogTimer.schedule(new TimerTask() {
    			@Override
    			public void run() {
    				runOnUiThread(new Runnable() {

    					@Override
    					public void run() {
    						closeMessageDialog();
    					}
    				});
    			}
    		}, 1000);        	
        }

    }
    
	public void getMacCode() {
		try{
			WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = manager.getConnectionInfo();
			String macaddress = info.getMacAddress();
			deviceCode = macaddress;
		}catch(Exception e) {
			//ErrorAlert("Error: " + e, false);		
		}
	} 
    
	public void closeMessageDialog() {

		if (dialogTimer != null) {
			dialogTimer.cancel();
			Intent i = new Intent(Flash.this, Geo_Main.class);
			startActivityForResult(i, 999);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);	
			finish();
			
		}
		
	}	 
	
	private void copyAssets() {
		
		//flashStatus.setVisibility(View.VISIBLE);
		//flashStatus.setText(getText(R.string.AssetFileList)); 
	    AssetManager assetManager = getAssets();
	    String[] files = null;
	    try {
	        files = assetManager.list("");
	    } catch (IOException e) {
			//flashStatus.setVisibility(View.VISIBLE);
			//flashStatus.setText(getText(R.string.AssetFileList1)); 
	    }
	    for(String filename : files) {
	        InputStream in = null;
	        OutputStream out = null;
	        try {
	          in = assetManager.open(filename);
	          File outFile = new File(getExternalFilesDir(null), filename);
	          out = new FileOutputStream(outFile);
	          copyFile(in, out);
	          in.close();
	          in = null;
	          out.flush();
	          out.close();
	          out = null;
	        } catch(IOException e) {

	        }       
	    }
	
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	    finishCopy = true;
	}	
	
//	private boolean isNetworkAvailable() {
//		
//	    ConnectivityManager connectivityManager 
//	          = (ConnectivityManager) Flash.this.getSystemService(Context.CONNECTIVITY_SERVICE);
//	    
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//	    
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//	    
//	}
	
	public void ErrorAlert(String AlertMsg, final Boolean showSettings)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Flash.this);
        builder.setTitle("TSQ-Geo Intel");
        builder.setMessage(AlertMsg);
        builder.setIcon(R.drawable.ic_launcher);
        
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(showSettings) {

				}
			}
		});	        

        builder.show();
    }	

	
}
