package com.tecsq.geointel;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.StrictMode;

import com.tecsq.Helper.Util;

public class WebSettingsListener extends Service {
	
	public final static String URL = "http://www.nmnglobal.com/tsqgeolicense/tsqgeo_activation.php?wsdl";
	public static final String NAMESPACE = "http://www.nmnglobal.com/";
	public static final String SOAP_ACTION_PREFIX = "/";
	public static String methodName = "";
	Boolean SoapError = false;	
    private Util util = new Util();
    private Context context;


    public WebSettingsListener() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
    @SuppressWarnings("deprecation")
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
        context = this;
		if (util.isNetworkAvailable(context)) {
			methodName="check_device_code";
			AsyncTaskRunner runner=new AsyncTaskRunner();
			runner.execute();
		}

    }
    
	private class AsyncTaskRunner extends AsyncTask<String,String,String>{
		private String err;
		@Override
		protected String doInBackground(String... params) {
			try{

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);              
				SoapObject request = new SoapObject(NAMESPACE, methodName);
				request.addProperty("device_code",Flash.deviceCode);
				
				envelope.setOutputSoapObject(request);              
				envelope.dotNet = true;
	
				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.call(NAMESPACE +  methodName, envelope);
				
				SoapObject result = (SoapObject)envelope.bodyIn;
				System.out.println(result);
				err = result.getProperty(0).toString();	

			} catch (SoapFault sf) {
				sf.printStackTrace();
				err = sf.getMessage();
				SoapError=true;
			} catch (Exception e) {
				e.printStackTrace();
				err = e.getMessage();
				SoapError=true;
			}
			
			return err;
		}
		
		protected void onPostExecute(String result){
			
			try {

				if(!SoapError){
					if(result.equalsIgnoreCase("no")) {

					} else {

					}
				} else {
					stopSelf();
				}
				
			} catch(Exception e) {

				stopSelf();
			}

		}
		
		@Override
		protected void onPreExecute() {	}

		@Override  
		protected void onProgressUpdate(String... text) { }
		
	}//end of runner
    
//	private boolean isNetworkAvailable() {
//
//
//		ConnectivityManager connectivityManager
//	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//
//	}

}


