package com.tecsq.geointel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.format.Time;

public class AppendRestarted {
    public String trimNow;
  	Time now = new Time();

	public void appendErrorLog(Context context, String text) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.getDeviceId();

		now.setToNow();
      	Calendar c = Calendar.getInstance(); 
      	Integer ms =  c.get(Calendar.MILLISECOND);
      	trimNow = now.toString().substring(0, 15) + "." + ms.toString();

		String trimAppendNow = trimNow.substring(0, 8);
		String logFileName = "geointel_" + telephonyManager.getDeviceId() + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
		}
	   try
	   {
		   String timeRestarted = "";
		   if (!trimNow.substring(0, 13).toString().equalsIgnoreCase(timeRestarted)) {
			   text = "," + "," + "," + trimNow + "," + "," + "," + "," + "Phone Restarted";
			   timeRestarted = trimNow.substring(0, 13);
			   BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
			   buf.append(text);
			   buf.newLine();
			   buf.close();
		   }
	   }
	   catch (IOException e)
	   {
	      e.printStackTrace();
	   }
	   catch (Exception e)
	   {
	      e.printStackTrace();
	   }

	}
	
}
