package com.tecsq.geointel;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.InputType;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tecsq.Helper.Constant;
import com.tecsq.Helper.OnLaunchProgressDialog;
import com.tecsq.Helper.Util;
import com.tecsq.database.Firebase_ActCode;
import com.tecsq.database.Firebase_Licenses;
import com.tecsq.filechecker.FileChecker;


public class Activation extends Dialog{

	private Context context;
	private String textActivationCode;
	private MaterialDialog mDialog = null;
	private Util util = new Util();
	private String imeiAddress;
	private String checkLicenseCode;
	private String[] fileName = {"geosense.tecgeo","tec.act"};

	public Activation(final Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		imeiAddress  = FileChecker.DeviceID(context);
		checkLicenseCode = util.readLicenseCode(context,fileName[1]);
		if(checkLicenseCode == "" || checkLicenseCode == null){
			showDialog();
		}
	}

	private void showDialog(){
		mDialog = new MaterialDialog.Builder(context)
		.title("TSQ-Geo Intel")
		.content("Please enter your activation code!")
		.inputType(InputType.TYPE_CLASS_TEXT)
		.input("Enter activation code","",new MaterialDialog.InputCallback(){
			@Override
			public void onInput(@NonNull MaterialDialog dialog, final CharSequence input) {
				if (util.isNetworkAvailable(context)) {
					textActivationCode = input.toString() ;
					Util.launchProgressDialog(context, "Please wait", "Authenticating Activation Code", new OnLaunchProgressDialog() {
						@Override
						public void onLaunch() {
							final Query queryref = Constant.mFirebaseDatabase.orderByChild("act_code").equalTo(textActivationCode);
							if(queryref != null){
								queryref.addValueEventListener(new ValueEventListener() {

									@Override
									public void onDataChange(DataSnapshot dataSnapshot) {
										if(dataSnapshot.getChildren() != null){
											onDataSnapshotChanged(dataSnapshot);
										}
									}

									@Override
									public void onCancelled(DatabaseError databaseError) {

									}
								});
							}else{
								System.out.println("License is Invalid");
								Util.ringProgressDialog.dismiss();
								util.ErrorAlert(context,"License is invalid!",true);
							}
						}
					});
				} else {
					util.ErrorAlert(context,"No Internet Connection Available!", false);
				}
			}
		}).show();
	}

	private void onDataSnapshotChanged(DataSnapshot dataSnapshot){
		try{
			if(dataSnapshot.getChildren() != null){
				Firebase_ActCode data;
				Firebase_Licenses licenses = null;
				for(DataSnapshot getData: dataSnapshot.getChildren()){
					if(getData != null){
						data = getData.getValue(Firebase_ActCode.class);
						licenses = new Firebase_Licenses(getData.getKey(),data);
					}
				}
				switch (licenses.fb_code.status){
					case 0:
						Constant.mFirebaseDatabase.child(licenses.key).child("status").setValue(1);
						Constant.mFirebaseDatabase.child(licenses.key).child("device_code").setValue(imeiAddress);
						util.saveActivationCode(context,textActivationCode,fileName[1]);
						util.ErrorAlert(context,"License Verified!",true);
						break;
					case 1:
						System.out.println("License is Already Used!");
						//util.ErrorAlert(context,"License is invalid!",false);
						break;
					case 2:
						System.out.println("License Already Expired");
						textActivationCode = "";
						util.saveActivationCode(context,textActivationCode,fileName[1]);
						util.ErrorAlert(context,"License is invalid!",true);
						break;
					default:
						System.out.println("License is invalid!");
						break;
				}
				Util.ringProgressDialog.dismiss();
			}
		}catch (NullPointerException e){
			System.out.println("License is Invalid");
			Util.ringProgressDialog.dismiss();
			util.ErrorAlert(context,"License is invalid!",true);
		}
	}

//	private String readLicenseCode(){
//		String sCurrentLine = null;
//		try {
//			File filename = new File(context.getExternalFilesDir(null),"tec.act");
//			BufferedReader br = new BufferedReader(new FileReader(filename));
//			String next = " ";
//
//			while((next = br.readLine()) != null){
//				sCurrentLine = next;
//			}
//			br.close();
//			return  sCurrentLine;
//
//		}catch (IOException e) {
//			e.printStackTrace();
//		}
//		return "";
//	}


//	private void addSaveButton(final Dialog dialog){
//
//		btnOk.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				progressIcon.setVisibility(View.VISIBLE);
//				if (isNetworkAvailable()) {
//					methodName="check_activation";
//					AsyncTaskRunner runner=new AsyncTaskRunner();
//					runner.execute();
//				} else {
//					ErrorAlert("No Internet Connection Available!", false);
//				}
//
//			}
//
//		});
//
//	}
	
//	private boolean isNetworkAvailable() {
//
//	    ConnectivityManager connectivityManager
//	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//
//	}
	
//	public void ErrorAlert(String AlertMsg, final Boolean showSettings)
//    {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
//        builder.setIcon(android.R.drawable.ic_dialog_alert);
//        builder.setTitle("TSQ-Geo Intel");
//        builder.setMessage(AlertMsg);
//        builder.setIcon(R.drawable.ic_launcher);
//
//		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				if(showSettings) {
////					progressIcon.setVisibility(View.INVISIBLE);
//					Intent refresh = new Intent(context, Geo_Main.class);
//					((Activity) context).finish();
//					context.startActivity(refresh);
//					mDialog.dismiss();
//				} else {
//					//progressIcon.setVisibility(View.INVISIBLE);
//					Intent refresh = new Intent(context, Geo_Main.class);
//					((Activity) context).finish();
//					context.startActivity(refresh);
//				}
//			}
//		});
//
//        builder.show();
//    }
	
//	@SuppressLint("DefaultLocale")
//	private class AsyncTaskRunner extends AsyncTask<String,String,String>{
//		private String err;
//		@SuppressLint("DefaultLocale")
//		@Override
//		protected String doInBackground(String... params) {
//			try{
//
//				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//				SoapObject request = new SoapObject(NAMESPACE, methodName);
//				request.addProperty("act_code",textActivationCode.toUpperCase());
//				request.addProperty("organization","TEST");
//				request.addProperty("device_code",myIMEI);
//
//				envelope.setOutputSoapObject(request);
//				envelope.dotNet = true;
//
//				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//				androidHttpTransport.call(NAMESPACE +  methodName, envelope);
//
//				SoapObject result = (SoapObject)envelope.bodyIn;
//				System.out.println(result);
//				err = result.getProperty(0).toString();
//
//			} catch (SoapFault sf) {
//				sf.printStackTrace();
//				err = sf.getMessage();
//				SoapError=true;
//			} catch (Exception e) {
//				e.printStackTrace();
//				err = e.getMessage();
//				SoapError=true;
//			}
//
//			return err;
//		}
//
//		protected void onPostExecute(String result){
//
//			try {
//
//				if(!SoapError){
//					System.out.println("activation code -----" + textActivationCode);
//					if(result.equalsIgnoreCase("no")) {
//						ErrorAlert("Invalid Activation Code!", false);
//					} else {
//						File filename = new File(context.getExternalFilesDir(null),"geosense.tecgeo");
//						if(filename.exists()) {
//							filename.delete();
//
//							//save Activation code org
//							saveActivationCode();
//							//progressIcon.setVisibility(View.INVISIBLE);
//							ErrorAlert("Geo Intel now activated! Thanks!", true);
//						}
//					}
//
//				} else {
//					ErrorAlert("Error Connecting to Activation Portal!", false);
//
//					//progressIcon.setVisibility(View.INVISIBLE);
//				}
//
//			} catch(Exception e) {
//				ErrorAlert("Error Connecting to Activation Portal!", false);
//			}
//
//		}
//
//		@Override
//		protected void onPreExecute() {	}
//
//		@Override
//		protected void onProgressUpdate(String... text) { }
//
//	}//end of runner
	
//	public void saveActivationCode(String text) {
//		try {
//			File file = new File(context.getExternalFilesDir(null),"tec.act");
//	        //if (!file.exists()) {
//			   file.createNewFile();
//	 		   BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
//			   buf.append(text);
//			   buf.close();
//	       // }
//		} catch(IOException e) {
//			e.printStackTrace();
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	}

}
