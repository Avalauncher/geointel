package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.util.Calendar;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tecsq.Helper.Util;
import com.tecsq.filechecker.FileChecker;

import android.annotation.SuppressLint;
//import android.annotation.TargetApi;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
//import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.format.Time;

@SuppressLint("NewApi")
public class FTPService extends Service {
	private Time now = new Time();
//	private String errTime ;
//	private String status;
//	ErrorLog errorLog = new ErrorLog(); 
//	private ftpErrorLog SendError = new ftpErrorLog();
//	Context context;
	
	private static final String BUCKET_NAME = "marketintelimages";
//	private static final String SFTP_PIC_DIR = "/var/www/market-intel/GeoIntel/pictures";
//	private static final String SFTPWORKINGDIR = "/var/www/market-intel/GeoIntel/geopoints";
//	private String sftpuser = "tecadmin"; //tecadmin, root
//	private String pass = "tSquar341"; //tSquar341, erkN8@)#hX.SPLD}
//	private String host = "marketintel.org";
//	private int portNum = 22;
//	private String sftpFileName = "";
//	private JSch jsch = new JSch();
//	private Session session = null;
//	private Channel channel = null;
//	private ChannelSftp sftpChannel = null;
//
//	private long sftpProgressCount = 0; //03042016Tony
//	private long sftpFileSize = 0;
	private long installedTime = 0;
	private long updateTime = 0;

	private Boolean sftpError = false;
	private String myImei;

	private TransferUtility transferUtility;
	public String trimNow;
	
	String currFileUpload;
	
	String sCurrentLine = null;
	String currFileDateTime;
	StorageReference mStorageRef;

	private Context context;
	private Util util = new Util();

	public FTPService() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
//		status = NetworkUtil.getConnectivityStatusString(this);
		context = this;
		transferUtility= Util.getTransferUtility(getApplicationContext());
		getActCode();
		try {
			installedTime = getBaseContext()
						.getPackageManager()
						.getPackageInfo(getBaseContext().getPackageName(), 0)
						.firstInstallTime;
			
			updateTime = getBaseContext()
					.getPackageManager()
					.getPackageInfo(getBaseContext().getPackageName(), 0)
					.lastUpdateTime ;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			//if (!ftpSend()) return;
			if (util.isNetworkAvailable(context)) {
				myImei = FileChecker.DeviceID(this);
				AsyncTaskRunner runner = new AsyncTaskRunner();
				runner.execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
			now.setToNow();
//			errTime = now.toString();
			//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": FTP (onstart) Send Service Error");
			//SendError.ftpSend(FTPService.this);
		}

		//now.setToNow();
		//errTime = now.toString();
		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + 
		//		": FTP Send Succesfully" + " : NetworkUsed: " + status);
		//SendError.ftpSend(FTPService.this);
		//		stopSelf();

	}

	public boolean ftpSend() {
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(getBaseContext());

		FTPClient mFTP = new FTPClient();
		try {
			//mFTP.connect("93.191.36.11");
			mFTP.connect("ftp.online-pmsi.com");
			mFTP.login("User127", "L22n1Pq4");
			//	    	  	mFTP.connect("194.109.6.98");
			//	    	  	mFTP.login("nolb", "Nmn670B57");
			mFTP.setDefaultTimeout(0);
			mFTP.setFileType(FTP.BINARY_FILE_TYPE);
			mFTP.enterLocalPassiveMode();

			// Prepare file to be uploaded to FTP Server
			Time now = new Time();
			now.setToNow();
			String trimNow = now.toString().substring(0, 8);
			//String logFileName = "geointel_" + myImei + "_" + trimNow + "_log" ;
			String logFileName = sCurrentLine + "_" + myImei + "_" + trimNow + "_log" ;
			File file = new File(Environment.getExternalStorageDirectory(), "/" + logFileName + ".txt");
			FileInputStream ifile = new FileInputStream(file);

			// Upload file to FTP Server
			mFTP.changeWorkingDirectory("GeoIntel");
			//	  			mFTP.changeWorkingDirectory("GeoIntel/test_logs");
			//	  			mFTP.changeWorkingDirectory("MarketIntel/sApp");
			if (!mFTP.storeFile(logFileName + ".txt", ifile)) {
				ifile.close();
				mFTP.disconnect();
				throw new Exception("Cannot Send File to FTP Server");
			}
			ifile.close();
			mFTP.disconnect();

		} catch (SocketException e) {
			sftpError = true;
			e.printStackTrace();
			now.setToNow();
//			errTime = now.toString();
			//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + 
			//		": FTP Socket Service Error:  "  + e + " : NetworkUsed: " + status);
			//SendError.ftpSend(FTPService.this);
			return false;
		} catch (Exception e) {
			sftpError = true;
			e.printStackTrace();
			now.setToNow();
//			errTime = now.toString();
			//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + 
			//		": FTP Service Error : "  + e + " : NetworkUsed: " + status);
			//SendError.ftpSend(FTPService.this);
			return false;
		} 

		return true;

	}

	private class AsyncTaskRunner extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				//				session = jsch.getSession(sftpuser, host, portNum);
				//				session.setConfig("StrictHostKeyChecking", "no");
				//				session.setPassword(pass);
				//				session.connect();
				//
				//				channel = session.openChannel("sftp");
				//				channel.connect();
				//				sftpChannel = (ChannelSftp) channel;
				//				sftpChannel.cd(SFTPWORKINGDIR);		
				//
				//				Time now = new Time();
				//				now.setToNow();
				//				String trimNow = now.toString().substring(0, 8);
				//				String logFileName = "geointel_" + myImei + "_" + trimNow + "_log" ;
				//				File file = new File(Environment.getExternalStorageDirectory(), "/" + logFileName + ".txt");
				//
				//				sftpChannel.put(new FileInputStream(file), file.getName());

				//ftpSend(); 2017-02-03Tony Commented to Check Firebase Storage Upload

			} catch (Exception e) {
				e.printStackTrace();
				sftpError = true;
				//pdBackCheck.dismiss();
			}

			return null;
		}

		protected void onPostExecute(String result){
//			session.disconnect();
			if (sftpError) {
				sftpError = false;
				AsyncTaskRunner runner = new AsyncTaskRunner();
				runner.execute();
				//UploadAlert("Error Uploading File to sftp! - " + result);
			} else {
				//UploadAlert("Upload File to sftp DONE! - " + result);
//				AsyncUploadPhotos asyncUploadPhotos = new AsyncUploadPhotos();
//				asyncUploadPhotos.execute();
				mStorageRef = FirebaseStorage.getInstance().getReference();
				uploadPhoto();
			}

		}

		protected void onPreExecute() { }

		protected void onProgressUpdate(String... text) { }

	}

	@SuppressWarnings("unused")
	private class AsyncUploadPhotos extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				uploadPhoto();
			} catch (Exception e) {
				e.printStackTrace();
				sftpError = true;
				//pdBackCheck.dismiss();
			}

			return null;
		}

		protected void onPostExecute(String result){ 
//			session.disconnect();
			if (sftpError) {
				sftpError = false;
				//UploadAlert("Error Uploading File to sftp! - " + result);
				AsyncUploadPhotos asyncUploadPhotos = new AsyncUploadPhotos();
				asyncUploadPhotos.execute();
			} else {
//				now.setToNow();
//	          	Calendar c = Calendar.getInstance(); 
//	          	Integer ms =  c.get(Calendar.MILLISECOND);
//				trimNow = now.toString().substring(0, 15) + "." + ms.toString();
//				appendLog("," + "," + "," + trimNow + "," + "," + "," + "," + "GPS Disabled");
				stopSelf();
			}
		}

	}

	private void uploadPhoto(){
		currFileUpload = "";
		Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED) && Environment.isExternalStorageRemovable();
		File directory = null;
		if(isSDPresent){
			File[] aDirArray = ContextCompat.getExternalFilesDirs(this, null);
			File path = new File(aDirArray.length> 1 ? aDirArray[1]:aDirArray[0], Environment.DIRECTORY_DCIM);
			String data = path.toString();
			int index = data.indexOf("Android");
			String subdata = data.substring(0, index);
			directory = new File(subdata + "DCIM" + "/Camera");
		}else{
			directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/Camera");
		}

		try{
			File[] entries = directory.listFiles();

			if (entries != null) {
				for (File entry : entries) {
					long lastModified = entry.lastModified();
					if (entry.isFile() && lastModified > installedTime ) {
						System.out.println("Entry is: " + entry.isFile() );
						System.out.println("installedTime: " + installedTime );
						System.out.println("entry.lastModified: " + lastModified );
						System.out.println("updateTime: " + updateTime );
						
						//dddd;
						//Boolean isPicture;
						if(entry.getName().substring(entry.getName().lastIndexOf(".") + 1).equalsIgnoreCase("jpg")) {
							

						
							String fileName = entry.getName();
	
							final int IMG_WIDTH = 1280;
							final int IMG_HEIGHT = 720;
							//Log.v("FILENAME SENDING TO FTP", fileName);
							// TO DO: filter the pic file to be send to ftp, should be the current respondent entry only/
							//								String respondID = fileName.substring(0, fileName.indexOf("_"));
							File fileUri = new File(directory, fileName);
	
							SystemClock.sleep(2000);
							
							Time now = new Time();
							now.setToNow();
							String trimNow = now.toString().substring(0, 15);
							String newFileName = "111_geointel_" + myImei + "_" + trimNow;
							currFileDateTime = trimNow;
							File fileRenamed = new File(directory, newFileName + ".jpg");
							fileUri.renameTo(fileRenamed);
							currFileUpload = newFileName;
							//Log.v("Fileuri", fileUri.toString());
							//Log.i("URI FILE",uriFile.toString());
	
							//Uri uriFile = Uri.fromFile(fileUri);
							Uri uriFile = Uri.fromFile(fileRenamed);
	
							System.out.println("Value of Image " + String.valueOf(fileRenamed));
	
							Bitmap b = BitmapFactory.decodeFile(String.valueOf(fileRenamed));
							Bitmap out = Bitmap.createScaledBitmap(b, IMG_WIDTH, IMG_HEIGHT, false);
							FileOutputStream fOut;
	
							try {
	
								fOut = new FileOutputStream(fileRenamed);
								out.compress(Bitmap.CompressFormat.JPEG, 60, fOut);
								fOut.flush();
								fOut.close();
								b.recycle();
								out.recycle();
								System.out.println("Compressing file " + String.valueOf(fileRenamed));
	
							} catch (Exception e) {
								e.printStackTrace();
							}
	
							try {
								String getUri = getPath(uriFile);
								beginUpload(getUri);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}					
					} //if entry is file

				}

			} else {
				stopSelf();
			}
		}catch(Exception e){
			e.printStackTrace();
			stopSelf();
		}

	}

	private String getPath(Uri uri) throws URISyntaxException {
		try{
			final boolean isKitKat  = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
			String selection = null;
			String[] selectionArgs = null;
			// Uri is different in versions after KITKAT (Android 4.4), we need to
			// deal with different Uris.
			if (isKitKat && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
				if (isMediaDocument(uri)) {
					final String docId = DocumentsContract.getDocumentId(uri);
					final String[] split = docId.split(":");
					final String type = split[0];
					if ("image".equals(type)) {
						uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
					} else if ("video".equals(type)) {
						uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
					} else if ("audio".equals(type)) {
						uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
					}
					selection = "_id=?";
					selectionArgs = new String[] {
							split[1]
					};

					return getDataColumn(getApplicationContext(), uri, selection, selectionArgs);
				}
			} else if ("content".equalsIgnoreCase(uri.getScheme())) {
				return getDataColumn(getApplicationContext(), uri, null, null);
			} else if ("file".equalsIgnoreCase(uri.getScheme())) {
				System.out.println("getPath return " + uri.getPath());
				return uri.getPath();
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
			String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {
				MediaStore.Images.Media.DATA
		};

		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
					null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
	
	public void geoTag(String filename){
		try{

			ExifInterface exif;

//			double latitude = 10.321360194822036, longitude= 123.93533335981842;
			//SLS 10042014
			double latitude = ServiceMain.globLatitude;
			double longitude= ServiceMain.globLongitude;

			exif = new ExifInterface(filename);
			int num1Lat = (int)Math.floor(latitude);
			int num2Lat = (int)Math.floor((latitude - num1Lat) * 60);
			double num3Lat = (latitude - ((double)num1Lat+((double)num2Lat/60))) * 3600000;

			int num1Lon = (int)Math.floor(longitude);
			int num2Lon = (int)Math.floor((longitude - num1Lon) * 60);
			double num3Lon = (longitude - ((double)num1Lon+((double)num2Lon/60))) * 3600000;

			exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, num1Lat+"/1,"+num2Lat+"/1,"+num3Lat+"/1000");
			exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, num1Lon+"/1,"+num2Lon+"/1,"+num3Lon+"/1000");

			if (latitude > 0) {
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
			} else {
				exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
			}

			if (longitude > 0) {
				exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
			} else {
				exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
			}
			
			exif.setAttribute(ExifInterface.TAG_DATETIME, currFileDateTime);
			exif.setAttribute(ExifInterface.TAG_GPS_TIMESTAMP, currFileDateTime);

			exif.saveAttributes();
		} catch (IOException e) {
			e.printStackTrace();
			//SendError.ftpSend(context);
		}

	}

	private void beginUpload(String filePath) {

		try{
			System.out.println("beginUpload " + filePath);
			if (filePath == null) {
				//	            Toast.makeText(this, "Could not find the filepath of the selected file",
				//	                    Toast.LENGTH_LONG).show();
				return;
			}
			geoTag(filePath);
			File file = new File(filePath);
			//Log.i("file.getName()",String.valueOf(file.getName()));
			//Log.i("file",String.valueOf(file));
			TransferObserver observer = transferUtility.upload(BUCKET_NAME, file.getName(),
					file);
			observer.setTransferListener(new UploadListener(file));


		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private class UploadListener implements TransferListener {

		File listenerFile;
		UploadListener(File fileName) {

			//MainMenu.responseProgress.setVisibility(View.VISIBLE);
			//MainMenu.responseProgress.setText("Uploading (" + awsFile + ")" );
			this.listenerFile = fileName;
		}

		@Override
		public void onError(int id, Exception e) {
			//MainMenu.responseProgress.setText("Error Uploading (" + awsFile + ")" );
			//ErrorAlert("Error in Uploading Photo/s!");
			stopSelf();
		}

		@Override
		public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

			//Log.i("OnUploAD","total" + bytesTotal + "--" + bytesCurrent);
			//MainMenu.responseProgress.setText("Uploading (" + awsFile + "): " + bytesCurrent + "/" + bytesTotal );

			if(bytesCurrent == bytesTotal){
				deleteSendPhoto(listenerFile);
				String picFileName;
				//String[] getFileName = listenerFile.toString().split("/");
				picFileName = listenerFile.toString().substring(listenerFile.toString().lastIndexOf("/") + 1);
				now.setToNow();
	          	Calendar c = Calendar.getInstance(); 
	          	Integer ms =  c.get(Calendar.MILLISECOND);
				trimNow = now.toString().substring(0, 15) + "." + ms.toString();
				appendLog("," + "," + "," + trimNow + "," + "," + "," + "," + "File Uploaded:" + picFileName);
			}

		}

		@Override
		public void onStateChanged(int id, TransferState newState) {
			//Log.i("Check state",newState.toString());
		}

	}

	private void deleteSendPhoto(File picName){

		if(picName.exists()) {

			picName.delete();

			String[] projection = { MediaStore.Images.Media._ID };


			String selection = MediaStore.Images.Media.DATA + " = ?";
			String[] selectionArgs = new String[] { picName.getAbsolutePath() };


			Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
			ContentResolver contentResolver = getContentResolver();
			Cursor c = contentResolver.query(queryUri, projection, selection, selectionArgs, null);
			if (c.moveToFirst()) {

				long id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Images.Media._ID));
				Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
				contentResolver.delete(deleteUri, null, null);
			}
			c.close();
		}

	}
//
//	private boolean isNetworkAvailable() {
//
//		ConnectivityManager connectivityManager
//		= (ConnectivityManager) FTPService.this.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//
//		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//
//	}
	
	public void appendLog(String text)
	{  
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(getBaseContext());	
		
      	String trimAppendNow = trimNow.substring(0, 8);
		//String logFileName = "geointel_" + FileChecker.DeviceID(FTPService.this) + "_" + trimAppendNow + "_log" ;
		String logFileName = sCurrentLine + "_" + FileChecker.DeviceID(FTPService.this) + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
	      catch (Exception e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }

		}
	   try
	   {
		   BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
		   buf.append(text);
		   buf.newLine();
		   buf.close();

	   }  catch (IOException e)  {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }  catch (Exception e)  {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	   }
	   
	}
	
	public void getActCode() {
    	
		try {
			File filename = new File(this.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
