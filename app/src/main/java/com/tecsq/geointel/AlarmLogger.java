package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.tecsq.filechecker.FileChecker;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.GpsStatus.NmeaListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.text.format.Time;
//import android.widget.Toast;

@SuppressLint({ "HandlerLeak", "SimpleDateFormat" })
public class AlarmLogger extends Service {
	public static Integer fix = 0;
	public static Integer satInView = 0;
	public static Double globLongitude = 0.0;
	public static Double globLatitude = 0.0;
	public static Float globAccuracy = 0f; 
    protected LocationManager locationManager;
    public static String satUTC;
    public static String utcDate;
    public boolean locChange;
    public String trimNow;
  	Time now = new Time();
  	public String currTime;
    public String nextTime;
    public static String currTimeAL;
    public static String logged;
    PendingIntent pendingLooper;
    
    String errTime;
	//ErrorLog errorLog = new ErrorLog(); 
	ftpErrorLog SendError = new ftpErrorLog();
	Context context;
	
	int red = R.drawable.sapp_red;
    int orange = R.drawable.sapp_orange;
    int green = R.drawable.sapp_green;
	int dis = R.drawable.sapp_grey;
    
	public AlarmLogger() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}
		
	@Override
    public void onCreate() {
    }
 

	@Override
    public void onStart(Intent intent, int startId) {

		if(FileChecker.isMyServiceRunning(ServiceMain.class, AlarmLogger.this)) {
			try {
				
				FileChecker fileChecker = new FileChecker(); 
				fileChecker.CheckCreateFile(getBaseContext());
				
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy); 
		
				Criteria crt = new Criteria(); 
				crt.setAccuracy(Criteria.ACCURACY_FINE);
			    
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				//600000, 400
		        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,60000, 400 ,new MyLocationListener());

		        locationManager.addNmeaListener(new NmeaListener() {
			    	public void onNmeaReceived(long timestamp, String nmea){
			    		
		            	final String sentence = nmea;
		            	
		    			if (sentence.startsWith("$GPRMC") == true) {
		    				String[] strValues = sentence.split(",");
		    				utcDate = strValues[9];
		    				satUTC = strValues[1];
		    			}
		
			    	}
		        });	

	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
			
		}
		
		
    }
    
    @Override
    public void onDestroy() {
    	try {
			  	Intent i = new Intent(Intent.ACTION_MAIN);
			  	i.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.Geo_Main"));
			  	i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			  	this.startActivity(i);
    	} catch(Exception e) {
    		e.printStackTrace();

    	}                
    }
    
    private class MyLocationListener implements LocationListener {
    	
		@Override
		public void onLocationChanged(Location location) {
			try {
				
				if (location!=null) {
					now.setToNow();
		          	Calendar c = Calendar.getInstance(); 
		          	Integer ms =  c.get(Calendar.MILLISECOND);
		          	trimNow = now.toString().substring(0, 15) + "." + ms.toString();
		          	
		          	globLatitude = location.getLatitude();
					globLongitude = location.getLongitude();
					globAccuracy = location.getAccuracy();
					locChange = true;
					
					if (globAccuracy <= 15f) {
						ServiceMain.notificationManager.cancel(2);
						ServiceMain.notificationManager.cancel(1);
						ServiceMain.notificationManager.cancel(4);
						showNotification(green,3);
					}
					if (globAccuracy > 15f && globAccuracy <= 50f) {
						ServiceMain.notificationManager.cancel(3);
						ServiceMain.notificationManager.cancel(1);
						ServiceMain.notificationManager.cancel(4);
						showNotification(orange,2);
					}					
					if (globAccuracy > 50f) {
						ServiceMain.notificationManager.cancel(3);
						ServiceMain.notificationManager.cancel(2);
						ServiceMain.notificationManager.cancel(4);
						showNotification(red,1);
					}		

			        String finSatUtcDate = utcDate;
			        String finSatUtcTime = satUTC;

					AlarmLogger.currTimeAL = now.toString().substring(0, 15);
			        String s1 = GPSSatInfo.satCount.toString();
			        String s2 = GPSSatInfo.satFix.toString();

			        String satDetails = "";
		          	
		          	if (finSatUtcTime.equals(null) || finSatUtcTime.equalsIgnoreCase("")) return;
		          	if (finSatUtcDate.equals(null) || finSatUtcDate.equalsIgnoreCase("")) return;

		          	if (!(finSatUtcDate.equals(null)) && !(finSatUtcTime.equals(null)))  {
		          		if (!(finSatUtcDate.equalsIgnoreCase("")) && !(finSatUtcTime.equalsIgnoreCase(""))) {
				          	satDetails  = isoFormat(finSatUtcDate, finSatUtcTime);
		          		}
		          	} else {
		          		satDetails  = "";
		          	}
		          	
		          	if(!satDetails.equalsIgnoreCase("")) {
		          		
						String preLog = satDetails + "," + s1 + "," + s2 + "," + AlarmLogger.currTimeAL;

						if (!(preLog.equalsIgnoreCase(logged))) {
							if (!(ServiceMain.loggedSM.equalsIgnoreCase(preLog))) {
								logged = satDetails + "," + s1 + "," + s2 + "," + AlarmLogger.currTimeAL;
								appendLog(logged + "." + ms.toString() + ","
					          			+ globLatitude + "," + globLongitude + "," + globAccuracy + ",");
							}
						}
		          	}
		          	//stopSelf();
				} else {
					ServiceMain.notificationManager.cancel(3);
					ServiceMain.notificationManager.cancel(2);
					ServiceMain.notificationManager.cancel(4);
					showNotification(red,1);
				}
	    	} catch(Exception e) {
	    		e.printStackTrace();
			  	//now.setToNow();
			  	//errTime = now.toString();
			  	//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": AL Location Error: " + e );
	    	}
		}

		public void onProviderDisabled(String s) {
			ServiceMain.notificationManager.cancel(3);
			ServiceMain.notificationManager.cancel(2);
			ServiceMain.notificationManager.cancel(1);
			showNotification(dis,4);
        }

        public void onProviderEnabled(String s) {
        	ServiceMain.notificationManager.cancel(3);
        	ServiceMain.notificationManager.cancel(2);
        	ServiceMain.notificationManager.cancel(4);
			showNotification(red,1);
        }

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		}

    }    
    
	public void appendLog(String text)
	{  
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(getBaseContext());
		
    	String sCurrentLine = null;
		try {
			File filename = new File(this.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
		
      	String trimAppendNow = trimNow.substring(0, 8);
		//String logFileName = "geointel_" + FileChecker.DeviceID(AlarmLogger.this) + "_" + trimAppendNow + "_log" ;
		String logFileName = sCurrentLine + "_" + FileChecker.DeviceID(AlarmLogger.this) + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
		}
	   try
	   {
		   BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
		   buf.append(text);
		   buf.newLine();
		   buf.close();
	   }
	   catch (IOException e)
	   {
	      e.printStackTrace();
	   }
	   catch (Exception e)
	   {
	      e.printStackTrace();
	   }
	}

		public String isoFormat(String Date, String Time) {
			
			SimpleDateFormat dateParser;
			SimpleDateFormat timeParser;
			
			SimpleDateFormat dateFormatter;
			SimpleDateFormat timeFormatter;
			
			Date parsedDate;
			Date parsedTime;
			
			try {
				
				dateParser = new SimpleDateFormat("ddMMyy");
				timeParser = new SimpleDateFormat("kkmmss.S");
				
				parsedDate = dateParser.parse(Date);
				parsedTime = timeParser.parse(Time);
			
				dateFormatter = new SimpleDateFormat("yyyyMMdd");
				timeFormatter = new SimpleDateFormat("'T'kkmmss.S");
				
				String str = (String) (dateFormatter.format(parsedDate) + "" + timeFormatter.format(parsedTime));
				
				 return str;	
			
			} catch (Exception e) {
				e.printStackTrace();
				Time isonow = new Time(now); 
				isonow.setToNow();
			  	//String errTime = isonow.toString();
			  	//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": ISOFormat Error (AL): " + e );
				//SendError.ftpSend(AlarmLogger.this);			  	
			  	return "";
			}
		}
		
	public void showNotification(int gpsColor, int notID){

		Notification mNotification = new Notification.Builder(this)
				.setContentTitle("Geo Intel Running!")
				.setSmallIcon(gpsColor)
				.build();

		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		ServiceMain.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		ServiceMain.notificationManager.notify(notID, mNotification);

	}

		
}
