package com.tecsq.geointel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.SoapFault;
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpTransportSE;

import com.tecsq.filechecker.FileChecker;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.AsyncTask;
import android.os.IBinder;
import android.os.StrictMode;

public class GeoIntelStopper extends Service {
	
	public final static String URL = "http://www.nmnglobal.com/tsqgeo_websettings/tsqgeo_websettings.php?wsdl";
	public static final String NAMESPACE = "http://www.nmnglobal.com/";
	public static final String SOAP_ACTION_PREFIX = "/";
	public static String methodName = "";
	Boolean SoapError = false;	
	String myImei;
	
	private Date devDate, runDate;
	public static final String inputFormat = "h:mm a";
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

    public GeoIntelStopper() {}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
    @SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 

		myImei = FileChecker.DeviceID(this);
//		if (isNetworkAvailable()) {
//			methodName="check_forceStop";
//			AsyncTaskRunner runner=new AsyncTaskRunner();
//			runner.execute();
//			//stopSAppServices();
//		}
		
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		String curr_date = df.format(Calendar.getInstance().getTime());
		String run_Date = "6:00 PM";
		runDate  = parseDate(run_Date);
		devDate = parseDate(curr_date);
		
		if(devDate.after(runDate)) {
			System.out.println("GeoIntelStopper-SHOULD FUCKING STOP THIS!!!");
			stopSAppServices();
		}

    }
    
    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
    
//	private class AsyncTaskRunner extends AsyncTask<String,String,String>{
//		private String err;
//		@Override
//		protected String doInBackground(String... params) {
//			try{
//
//				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);              
//				SoapObject request = new SoapObject(NAMESPACE, methodName);
//				request.addProperty("device_code",myImei);
//				
//				envelope.setOutputSoapObject(request);              
//				envelope.dotNet = true;
//	
//				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//				androidHttpTransport.call(NAMESPACE +  methodName, envelope);
//				
//				SoapObject result = (SoapObject)envelope.bodyIn;
//				System.out.println(result);
//				err = result.getProperty(0).toString();	
//
//			} catch (SoapFault sf) {
//				sf.printStackTrace();
//				err = sf.getMessage();
//				SoapError=true;
//			} catch (Exception e) {
//				e.printStackTrace();
//				err = e.getMessage();
//				SoapError=true;
//			}
//			
//			return err;
//		}
//		
//		protected void onPostExecute(String result){
//			
//			try {
//
//				if(!SoapError){
//					if(result.equalsIgnoreCase("stop")) {
//						//this.stopService(new Intent(this, FTPService.class));
//						stopSAppServices();
//					} 
//				} else {
//					stopSelf();
//				}
//				
//			} catch(Exception e) {
//
//				stopSelf();
//			}
//
//		}
//		
//		@Override
//		protected void onPreExecute() {	}
//
//		@Override  
//		protected void onProgressUpdate(String... text) { }
//		
//	}//end of runner
	
	
	public void stopSAppServices() {
		
    	try {
    		this.stopService(new Intent(this, FTPService.class));
    		this.stopService(new Intent(this, ServiceMain.class));
    		this.stopService(new Intent(this, AlarmLogger.class));
    		this.stopService(new Intent(this, GPSSatInfo.class));
    		this.stopService(new Intent(this, GPSStatus.class));
    		this.stopService(new Intent(this, LocationService_Checker.class));
    		this.stopService(new Intent(this, Activation_Checker.class));
    		
    		String ns = Context.NOTIFICATION_SERVICE;
    	    NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
    	    nMgr.cancel(36987);
    	    
    	    int pid = android.os.Process.myPid();
    	    android.os.Process.killProcess(pid);
    	    Intent intent = new Intent(Intent.ACTION_MAIN);
    	    intent.addCategory(Intent.CATEGORY_HOME);
    	    startActivity(intent);

    	    stopSelf();

    	} catch(Exception e) {
    		e.printStackTrace();
    	}
		
	}
    
//	private boolean isNetworkAvailable() {
//		
//	    
//		ConnectivityManager connectivityManager 
//	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//	    
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//	    
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//	    
//	}

}


