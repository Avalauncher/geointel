package com.tecsq.geointel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tecsq.Helper.Constant;
import com.tecsq.Helper.Util;
import com.tecsq.database.Firebase_ActCode;
import com.tecsq.database.Firebase_Licenses;
import com.tecsq.filechecker.FileChecker;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class Activation_Act extends Activity{
	
	private Button btnOk;
	private ProgressBar progressIcon;
	private EditText editActivation;
//	public final static String URL = "http://www.nmnglobal.com/tsqgeolicense/tsqgeo_activation.php?wsdl";
//	public static final String NAMESPACE = "http://www.nmnglobal.com/";
//	public static final String SOAP_ACTION_PREFIX = "/";
//	public static String methodName = "";
	Boolean SoapError = false;
	String myIMEI;
	private Util util = new Util();
	private Context context;
	private String textActivationCode;
	private String[] fileName = {"geosense.tecgeo","tec.act"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activation_act);
		context = this;
		textActivationCode = util.readLicenseCode(context,fileName[1]);
		btnOk = (Button) findViewById(R.id.button1);
		editActivation = (EditText) findViewById(R.id.editText1);
		progressIcon  = (ProgressBar) findViewById(R.id.progressBar1);
		progressIcon.setVisibility(View.INVISIBLE);
		myIMEI = FileChecker.DeviceID(context);
		addListenerOnButton();
	}
	
	
	public void addListenerOnButton(){
		btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				progressIcon.setVisibility(View.VISIBLE);
				if (util.isNetworkAvailable(context)) {
					final Query queryRef = Constant.mFirebaseDatabase.orderByChild("act_code").equalTo(textActivationCode);
					if(queryRef != null){
						queryRef.addValueEventListener(new ValueEventListener() {
							@Override
							public void onDataChange(DataSnapshot dataSnapshot) {
								onDatasnapshotChanged(dataSnapshot);
							}

							@Override
							public void onCancelled(DatabaseError databaseError) {

							}
						});
					}
//					methodName="check_activation";
//					AsyncTaskRunner runner=new AsyncTaskRunner();
//					runner.execute();
				} else {
					ErrorAlert("No Internet Connection Available!", false);
				}
			}

		});
    }

    private void onDatasnapshotChanged(DataSnapshot dataSnapshot){
		try{
			if(dataSnapshot.getChildren() != null){
				Firebase_ActCode data;
				Firebase_Licenses licenses = null;
				for(DataSnapshot getData: dataSnapshot.getChildren()){
					if(getData != null){
						data = getData.getValue(Firebase_ActCode.class);
						licenses = new Firebase_Licenses(getData.getKey(),data);
					}
				}
				switch (licenses.fb_code.status){
					case 0:
						Constant.mFirebaseDatabase.child(licenses.key).child("status").setValue(1);
						Constant.mFirebaseDatabase.child(licenses.key).child("device_code").setValue(myIMEI);
						util.saveActivationCode(context,textActivationCode,fileName[1]);
						ErrorAlert("License Verified!",true);
						break;
					case 1:
						System.out.println("License is Already Used!");
						//util.ErrorAlert(context,"License is invalid!",false);
						break;
					case 2:
						System.out.println("License Already Expired");
						textActivationCode = "";
						util.saveActivationCode(context,textActivationCode,fileName[1]);
						ErrorAlert("License is invalid!",true);
						//textActivationCode = "";
						//util.saveActivationCode(context,textActivationCode,fileName[1]);
						break;
					default:
						System.out.println("License is invalid!");
						break;
				}
				Util.ringProgressDialog.dismiss();
			}
		}catch (NullPointerException e){
			System.out.println("License is Invalid");
			Util.ringProgressDialog.dismiss();
			util.ErrorAlert(context,"License is invalid!",true);
		}
	}
	
//	private boolean isNetworkAvailable() {
//
//	    ConnectivityManager connectivityManager
//	          = (ConnectivityManager) Activation_Act.this.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//
//	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//
//	}
	
	public void ErrorAlert(String AlertMsg, final Boolean showSettings)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(Activation_Act.this);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("TSQ-Geo Intel");
        builder.setMessage(AlertMsg);
        builder.setIcon(R.drawable.ic_launcher);
        
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(showSettings) {
					progressIcon.setVisibility(View.INVISIBLE);
					Intent refresh = new Intent(Activation_Act.this, Geo_Main.class);
					finish();
					Activation_Act.this.startActivity(refresh);					

				} else {
					progressIcon.setVisibility(View.INVISIBLE);
				}
			}
		});	        

        builder.show();
    }	
	
	
//	private class AsyncTaskRunner extends AsyncTask<String,String,String>{
//		private String err;
//		@SuppressLint("DefaultLocale")
//		@Override
//		protected String doInBackground(String... params) {
//			try{
//
//				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//				SoapObject request = new SoapObject(NAMESPACE, methodName);
//				request.addProperty("act_code",editActivation.getText().toString().toUpperCase());
//				//request.addProperty("organization",editCompany.getText().toString());
//				request.addProperty("organization","TEST");
//				request.addProperty("device_code",myIMEI);
//
//				envelope.setOutputSoapObject(request);
//				envelope.dotNet = true;
//
//				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//				androidHttpTransport.call(NAMESPACE +  methodName, envelope);
//
//				SoapObject result = (SoapObject)envelope.bodyIn;
//				System.out.println(result);
//				err = result.getProperty(0).toString();
//
//			} catch (SoapFault sf) {
//				sf.printStackTrace();
//				err = sf.getMessage();
//				SoapError=true;
//			} catch (Exception e) {
//				e.printStackTrace();
//				err = e.getMessage();
//				SoapError=true;
//			}
//
//			return err;
//		}
//
//		protected void onPostExecute(String result){
//
//			try {
//
//				if(!SoapError){
//
//					if(result.equalsIgnoreCase("no")) {
//						ErrorAlert("Invalid Activation Code!", false);
//					} else {
//
//						saveActivationCode();
//						ErrorAlert("Geo Intel now activated! Thanks!", true);
//					}
//
//				} else {
//					ErrorAlert("Error Connecting to Activation Portal!", false);
//
//					progressIcon.setVisibility(View.INVISIBLE);
//				}
//
//			} catch(Exception e) {
//				ErrorAlert("Error Connecting to Activation Portal!", false);
//			}
//
//		}
//
//		@Override
//		protected void onPreExecute() {	}
//
//		@Override
//		protected void onProgressUpdate(String... text) { }
//
//	}//end of runner
	
//	@SuppressLint("DefaultLocale")
//	public void saveActivationCode() {
//		try {
//			File file = new File(this.getExternalFilesDir(null),"tec.act");
//	        if (!file.exists()) {
//	            file.createNewFile();
//
//	 		   BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
//			   buf.append(editActivation.getText().toString().toUpperCase());
//			   buf.newLine();
//			   buf.close();
//	        }
//		} catch(IOException e) {
//			e.printStackTrace();
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//
//	}

	
}
