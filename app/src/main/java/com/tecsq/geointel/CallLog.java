package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import com.tecsq.filechecker.FileChecker;
import android.content.Context;
import android.os.Environment;
import android.text.format.Time;

public class CallLog {
	
	public String trimNow;
	Time now = new Time();
	



	public void appendErrorLog(Context context, String text) {
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(context);
		
		
		now.setToNow();
      	Calendar c = Calendar.getInstance(); 
      	Integer ms =  c.get(Calendar.MILLISECOND);
      	trimNow = now.toString().substring(0, 15) + "." + ms.toString();
		
    	String sCurrentLine = null;
		try {
			File filename = new File(context.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}
		catch (IOException e) {
			e.printStackTrace();
		}

      	String trimAppendNow = trimNow.substring(0, 8);
		//String logFileName = "geointel_" + FileChecker.DeviceID(ServiceMain.this) + "_" + trimAppendNow + "_log" ;

      	String logFileName = sCurrentLine + "_" + FileChecker.DeviceID(context) + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
	      catch (Exception e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }

		}
	   try
	   {
		   BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
		   buf.append(text);
		   buf.newLine();
		   buf.close();

	   }  catch (IOException e)  {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }  catch (Exception e)  {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	   }
	

	}
}
