package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import com.tecsq.filechecker.FileChecker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Environment;
import android.os.IBinder;
import android.text.format.Time;

public class GPSStatus extends Service{
    public String trimNow;
  	Time now = new Time();
  	String errTime;
	//ErrorLog errorLog = new ErrorLog(); 
	ftpErrorLog SendError = new ftpErrorLog();
	protected LocationManager locationManager;
	
	Context context;
	
	int red = R.drawable.sapp_red;
	int dis = R.drawable.sapp_grey;
	
  	public GPSStatus() {
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not yet implemented");
		
	}
	@Override
    public void onCreate() { }

	@Override
    public void onStart(Intent intent, int startId) {
		
		if(FileChecker.isMyServiceRunning(ServiceMain.class, GPSStatus.this)) {
			try {
				now.setToNow();
	          	Calendar c = Calendar.getInstance(); 
	          	Integer ms =  c.get(Calendar.MILLISECOND);
	          	trimNow = now.toString().substring(0, 15) + "." + ms.toString();

				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				Boolean isGPSEnabled =  locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

				String timeDisabled = ""; 
				if (!isGPSEnabled) {
					gpsDis();
					if (!trimNow.substring(0, 13).toString().equalsIgnoreCase(timeDisabled)) {
			          	appendLog("," + "," + "," + trimNow + "," + "," + "," + "," + "GPS Disabled");
			          	timeDisabled = trimNow.substring(4, 16);
					}
				}

				String timeNoGPS = ""; 
				if ((GPSSatInfo.satFix <=0 || GPSSatInfo.satFix == null) && isGPSEnabled) { 
					noGPSLock();
					if (!trimNow.substring(0, 13).toString().equalsIgnoreCase(timeNoGPS)) {
						appendLog( "," + "," + "," + trimNow + "," + "," + "," + "," + "No GPS lock");
			          	timeNoGPS = trimNow.substring(4,16);
						
					}

				}		
				stopSelf();
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
	}

	
	public void appendLog(String text)
	{  
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(getBaseContext());
		
    	String sCurrentLine = null;
		try {
			File filename = new File(this.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}
		catch (IOException e) {
			e.printStackTrace();
		}		
		
      	String trimAppendNow = trimNow.substring(0, 8);
		//String logFileName = "geointel_" + FileChecker.DeviceID(GPSStatus.this) + "_" + trimAppendNow + "_log" ;
		String logFileName = sCurrentLine + "_" + FileChecker.DeviceID(GPSStatus.this) + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
		}
	   try
	   {
	      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
	      buf.append(text);
	      buf.newLine();
	      buf.close();
	   }
	   catch (IOException e)
	   {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }
	}
	
	public void showNotification(int gpsColor, int notID){

		Notification mNotification = new Notification.Builder(this)
				.setContentTitle("Geo Intel Running!")
				.setSmallIcon(gpsColor)
				.build();

		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		ServiceMain.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		ServiceMain.notificationManager.notify(notID, mNotification);

	}
    
    public void noGPSLock() {
		ServiceMain.notificationManager.cancel(3);
		ServiceMain.notificationManager.cancel(2);
		ServiceMain.notificationManager.cancel(4);
		showNotification(red,1);
	}
	
	public void gpsDis() {
		ServiceMain.notificationManager.cancel(3);
		ServiceMain.notificationManager.cancel(2);
		ServiceMain.notificationManager.cancel(1);
		showNotification(dis,4);
	}
	
}
