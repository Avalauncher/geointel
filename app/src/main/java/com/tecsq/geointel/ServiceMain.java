package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.tecsq.filechecker.FileChecker;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
//import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.GpsStatus.NmeaListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.SystemClock;
import android.text.format.Time;

@SuppressLint({ "HandlerLeak", "SimpleDateFormat" })
public class ServiceMain extends Service {
	public static Integer fix = 0;
	public static Integer satInView = 0;
	public static Double globLongitude = 0.0;
	public static Double globLatitude = 0.0;
	public static Float globAccuracy = 0f; 
    protected LocationManager locationManager;
    private PendingIntent pendingActivationChecker, pendingAlarmLogger, pendingGPSStatus, pendingLocation, pendingStopMe;
    public static String satUTC;
    public static String utcDate;
    public boolean locChange;
    public String trimNow;
  	Time now = new Time();
  	String errTime;
	//ErrorLog errorLog = new ErrorLog(); 
	ftpErrorLog SendError = new ftpErrorLog();
  	public String currTime;
    public String nextTime;
    public static String loggedSM;
    
    Context context;
    public static NotificationManager notificationManager;
    int red = R.drawable.sapp_red;
	int orange = R.drawable.sapp_orange;
	int green = R.drawable.sapp_green;
	int dis = R.drawable.sapp_grey;
	
	AlarmManager alarmLocation;
	AlarmManager alarmGPSStatus;
	AlarmManager alarmAlarmLogger;
	AlarmManager alarmActivationChecker;
	AlarmManager alarmLogger;
	AlarmManager alarmStopme;
    
    public ServiceMain() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}
		
	@Override
    public void onCreate() {
        //Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
		showNotification(red,1);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
    public void onStart(Intent intent, int startId) {
		
    	try {
    		
			FileChecker fileChecker = new FileChecker(); 
			fileChecker.CheckCreateFile(getBaseContext());
			
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy); 
    		
	        Intent myIntentLocation = new Intent(ServiceMain.this, LocationService_Checker.class);
			alarmLocation = (AlarmManager)getSystemService(ALARM_SERVICE);
	        pendingLocation = PendingIntent.getService(ServiceMain.this, 445454, myIntentLocation, 0);
	        alarmLocation.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000,pendingLocation);    		
    		
			Intent serviceInfo = new Intent(ServiceMain.this, GPSSatInfo.class);
			ServiceMain.this.startService(serviceInfo);
			
			if (GPSSatInfo.satFix <=0 || GPSSatInfo.satFix == null) {
		        Intent myIntent1Minute = new Intent(ServiceMain.this, GPSStatus.class);
		        alarmGPSStatus = (AlarmManager)getSystemService(ALARM_SERVICE);
		        pendingGPSStatus = PendingIntent.getService(ServiceMain.this, 445454, myIntent1Minute, 0);
		        alarmGPSStatus.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000,pendingGPSStatus);
			}
	        
	        Intent myIntent = new Intent(ServiceMain.this, Activation_Checker.class);
	        alarmActivationChecker = (AlarmManager)getSystemService(ALARM_SERVICE);
	        pendingActivationChecker = PendingIntent.getService(ServiceMain.this, 445454, myIntent, 0);
	        alarmActivationChecker.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 2000 , pendingActivationChecker);


			
			//time check interval for ftpSend
				//using alarm manager
//		        Intent myIntent = new Intent(ServiceMain.this, FTPService.class);
//		        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
//		        pendingIntent = PendingIntent.getService(ServiceMain.this, 445454, myIntent, 0);
//		        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 300000 , pendingIntent);
		        //using alarm manager end	
	
		        Criteria crt = new Criteria(); 
		        crt.setAccuracy(Criteria.ACCURACY_FINE);
		        
		        
		        
		        
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			//provider, minTime(0 = disabled), minDistance in Meters //0, 10
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0, 4,new MyLocationListener());
	
	        locationManager.addNmeaListener(new NmeaListener() {
		    	public void onNmeaReceived(long timestamp, String nmea){
		    		
	            	final String sentence = nmea;
	            	
	    			if (sentence.startsWith("$GPRMC") == true) {
	    				String[] strValues = sentence.split(",");
	    				utcDate = strValues[9];
	    				satUTC = strValues[1];
	    			}
	            	
		    	}
	        });	
	        
	    	SystemClock.sleep(2000);
	        Intent myIntent1Minute = new Intent(ServiceMain.this, AlarmLogger.class);
	        alarmLogger = (AlarmManager)getSystemService(ALARM_SERVICE);
	        pendingAlarmLogger = PendingIntent.getService(ServiceMain.this, 445454, myIntent1Minute, 0);
	        alarmLogger.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, pendingAlarmLogger);
    	
    	} catch(Exception e) {
    		e.printStackTrace();
		  	now.setToNow();
		  	errTime = now.toString();
    		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": onStart ServiceMain Error: " + e);
    		//SendError.ftpSend(ServiceMain.this);    	
    	}

    	try {
    		
        Intent myIntent1Minute = new Intent(ServiceMain.this, StopMe.class);
        alarmStopme = (AlarmManager)getSystemService(ALARM_SERVICE);
        pendingStopMe = PendingIntent.getService(ServiceMain.this, 445454, myIntent1Minute, 0);
        alarmStopme.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5000, pendingStopMe);
    	} catch (Exception e){
    		e.printStackTrace();
    	}

    }
    
    public void showNotification(int gpsColor, int notID){

		Notification mNotification = new Notification.Builder(this)
				.setContentTitle("Geo Intel Running!")
				.setSmallIcon(gpsColor)
				.build();

		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(notID, mNotification);

	}
 
    @Override
    public void onDestroy() {
    	try {
			notificationManager.cancel(0);
			notificationManager.cancel(1);
			notificationManager.cancel(2);
			notificationManager.cancel(3);
			notificationManager.cancel(4);
		} catch(Exception e) {
			e.printStackTrace();
		}
		alarmGPSStatus.cancel(pendingGPSStatus);
		alarmAlarmLogger.cancel(pendingAlarmLogger);
		alarmLocation.cancel(pendingLocation);
		alarmActivationChecker.cancel(pendingActivationChecker);
		alarmLogger.cancel(pendingAlarmLogger);
		alarmStopme.cancel(pendingStopMe);
    	try {
	    	Intent i = new Intent(Intent.ACTION_MAIN);
	    	i.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.Geo_Main"));
	        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        this.startActivity(i);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private class MyLocationListener implements LocationListener {
    	
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub

			if (location!=null) {
				try {
					
					now.setToNow();
		          	Calendar c = Calendar.getInstance(); 
		          	Integer ms =  c.get(Calendar.MILLISECOND);
		          	trimNow = now.toString().substring(0, 15) + "." + ms.toString();
		          	
					ServiceMain.globLatitude = location.getLatitude();
					ServiceMain.globLongitude = location.getLongitude();
					ServiceMain.globAccuracy = location.getAccuracy();
					
					if(globAccuracy <= 15f) {
						notificationManager.cancel(2);
						notificationManager.cancel(1);
						notificationManager.cancel(4);
						showNotification(green,3);
					}
					if(globAccuracy > 15f && globAccuracy <= 50f) {
						notificationManager.cancel(3);
						notificationManager.cancel(1);
						notificationManager.cancel(4);

						showNotification(orange,2);
					}
					if(globAccuracy > 50f) {
						notificationManager.cancel(3);
						notificationManager.cancel(2);
						notificationManager.cancel(4);
						showNotification(red,1);
					}
					
					locChange = true;
					String currTimeSM = now.toString().substring(0, 15);
					
			        String finSatUtcDate = utcDate;
			        String finSatUtcTime = satUTC;

		          	String s1 = GPSSatInfo.satCount.toString();
		          	String s2 = GPSSatInfo.satFix.toString();
		          	
		          	String satDetails = "";
		          	
		          	if (finSatUtcTime.equals(null) || finSatUtcTime.equalsIgnoreCase("")) return;
		          	if (finSatUtcDate.equals(null) || finSatUtcDate.equalsIgnoreCase("")) return;

		          	if (!(finSatUtcDate.equals(null)) && !(finSatUtcTime.equals(null)))  {
		          		if (!(finSatUtcDate.equalsIgnoreCase("")) && !(finSatUtcTime.equalsIgnoreCase(""))) {
				          	satDetails  = isoFormat(finSatUtcDate, finSatUtcTime);
		          		}
		          	} else {
		          		satDetails  = "";
		          	}

		          	if(!satDetails.equalsIgnoreCase("")) {
			          	if (!(currTimeSM.equalsIgnoreCase(AlarmLogger.currTimeAL))) {
			          		ServiceMain.loggedSM = satDetails + "," + s1 + "," + s2 + "," + currTimeSM;
			          		appendLog(loggedSM + "." + ms.toString() + ","
										+ globLatitude + "," + globLongitude + "," + globAccuracy + ",");	    	
			          	} 
		          	}

				} catch(Exception e) {
					e.printStackTrace();
				  	now.setToNow();
				  	errTime = now.toString();
		    		//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": ServiceMain Location Error : " + e);
		    		//SendError.ftpSend(ServiceMain.this);
				}
			} else {
				notificationManager.cancel(3);
				notificationManager.cancel(2);
				notificationManager.cancel(4);
				showNotification(red,1);
			}

		}

		public void onProviderDisabled(String s) {
			notificationManager.cancel(3);
			notificationManager.cancel(2);
			notificationManager.cancel(1);
			showNotification(dis,4);
        }

        public void onProviderEnabled(String s) {
        	notificationManager.cancel(3);
			notificationManager.cancel(2);
			notificationManager.cancel(4);
			showNotification(red,1);
        }

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			// TODO Auto-generated method stub
		
		}

    }    

	public void appendLog(String text)
	{  
		FileChecker fileChecker = new FileChecker(); 
		fileChecker.CheckCreateFile(getBaseContext());
		
		
    	String sCurrentLine = null;
		try {
			File filename = new File(this.getExternalFilesDir(null),"tec.act");
		    BufferedReader br = new BufferedReader(new FileReader(filename));
		    	
		   
		    String next = " ";	
	   
	
		    //String []message = null
		 
		    //read the last line of the file  
		   while((next = br.readLine()) != null){
		        sCurrentLine = next;
		        //message = sCurrentLine.split(":");
		   }
		   
	       br.close();
	       
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
      	String trimAppendNow = trimNow.substring(0, 8);
		//String logFileName = "geointel_" + FileChecker.DeviceID(ServiceMain.this) + "_" + trimAppendNow + "_log" ;
		String logFileName = sCurrentLine + "_" + FileChecker.DeviceID(ServiceMain.this) + "_" + trimAppendNow + "_log" ;
		File logFile = new File(Environment.getExternalStorageDirectory(), logFileName + ".txt");
		if (!logFile.exists())
		{
	      try
	      {
	    	  logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
	      catch (Exception e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }

		}
	   try
	   {
		   BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
		   buf.append(text);
		   buf.newLine();
		   buf.close();

	   }  catch (IOException e)  {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }  catch (Exception e)  {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	   }
	   
	}

		public String isoFormat(String Date, String Time) {
			
			SimpleDateFormat dateParser;
			SimpleDateFormat timeParser;
			
			SimpleDateFormat dateFormatter;
			SimpleDateFormat timeFormatter;
			
			Date parsedDate;
			Date parsedTime;
			
			try {
				
				dateParser = new SimpleDateFormat("ddMMyy");
				timeParser = new SimpleDateFormat("kkmmss.S");
				
				parsedDate = dateParser.parse(Date);
				parsedTime = timeParser.parse(Time);
				
				dateFormatter = new SimpleDateFormat("yyyyMMdd");
				timeFormatter = new SimpleDateFormat("'T'kkmmss.S");
				
				 String str = (String) (dateFormatter.format(parsedDate) + "" + timeFormatter.format(parsedTime));
			
				 return str;	
			
			} catch (Exception e) {
				e.printStackTrace();
				Time isonow = new Time(now); 
				isonow.setToNow();
			  	//String errTime = isonow.toString();
			  	//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": ISOFormat Error(ServiceMain): " + e );
	    		//SendError.ftpSend(ServiceMain.this);
	    		return "";
			}
		}
		
}
