package com.tecsq.geointel;

import java.util.Iterator;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.text.format.Time;

public class GPSSatInfo extends Service{
    public String trimNow;
  	Time now = new Time();
  	String errTime;
	//ErrorLog errorLog = new ErrorLog(); 
	ftpErrorLog SendError = new ftpErrorLog();
	public static Integer satCount = 0;
	public static Integer satFix = 0;
    protected LocationManager locationManager;
    private static final float MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 2; // 2 in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 0; // in Milliseconds // 5 Seconds
    
    public GPSSatInfo() {
    	
    }
  	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not yet implemented");
		
	}
	@Override
    public void onCreate() { }

	@Override
    public void onStart(Intent intent, int startId) {
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy); 
	
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	        locationManager.requestLocationUpdates( 
	                LocationManager.GPS_PROVIDER,  
	                MINIMUM_TIME_BETWEEN_UPDATES, 
	                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES ,
	                new MyLocationListener()
	        );
		
			locationManager.addGpsStatusListener(new MyGPSListener());    		
		} catch(Exception e) {
			e.printStackTrace();
		  	now.setToNow();
		  	errTime = now.toString();
			//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": SatInfo onStart Error: " + e);			
			//SendError.ftpSend(GPSSatInfo.this);		
		}	
	}
	
    @Override
    public void onDestroy() {
    	try {
	    	Intent i = new Intent(Intent.ACTION_MAIN);
	    	i.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.Geo_Main"));
	        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        this.startActivity(i);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }

	
	 private class MyLocationListener implements LocationListener {
	    	
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderDisabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
	 }
	 
	    public class MyGPSListener implements GpsStatus.Listener {
	    	@Override
	        public void onGpsStatusChanged(int event) {

	    		try {

	    			GpsStatus gpsStatus = locationManager.getGpsStatus(null);
		    		
		    		if(gpsStatus != null) {
		                Iterable<GpsSatellite>satellites = gpsStatus.getSatellites();
		                Iterator<GpsSatellite>sat = satellites.iterator();
		                GPSSatInfo.satFix = 0;
		                GPSSatInfo.satCount = 0;
		                while (sat.hasNext()) {
		                    GpsSatellite satellite = sat.next();
		                    if (satellite.usedInFix()) {
		                    	GPSSatInfo.satFix = GPSSatInfo.satFix +1;
		                    }
		                    GPSSatInfo.satCount = GPSSatInfo.satCount + 1;
		                }
		                
		            }

		    	} catch(Exception e) {
		    		e.printStackTrace();
				  	now.setToNow();
				  	errTime = now.toString();
					//errorLog.appendErrorLog(getBaseContext(),errTime.substring(0, 15) + ": SatInfo Listener Error");			
					//SendError.ftpSend(GPSSatInfo.this);			    	
		    	}
	    	}
	    }
	    

}
