package com.tecsq.geointel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;

public class StopMe extends Service {

    public StopMe() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
    public void onStart(Intent intent, int startId) {
    	String stopBa = getbaseUrl(StopMe.this);
    	if(stopBa.equalsIgnoreCase("stop")) {
    		android.os.Process.killProcess(android.os.Process.myPid());
    	}
    		
    }
	
	public static String getbaseUrl(Context context) {
		   String baseUrl = "";

		    File filename = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/" + "/com.nmn.mi-v2-beta" + "/files" + "/stopSapp.nmn");
			try {
			    BufferedReader br = new BufferedReader(new FileReader(filename));
			    String sCurrentLine = null;	
			    String next = " ";			
			    int lineNumber = 0;			
			    String []message=null;
			   
			    while((next = br.readLine()) != null){
		    	    lineNumber++;
		    	    if(lineNumber == 1){  //read line #1
		    	        sCurrentLine = next;
		    	        break;
		    	    }
		    	}
			    
		       if (sCurrentLine != null) {
		    	  message = sCurrentLine.split(",");
		    	}
			  
			   baseUrl = message[0].toString(); 
			   //baseUrl = message[0].toString();
			   br.close();
		     
		       } catch (IOException e) {
		               e.printStackTrace();
		       }
		       return baseUrl;
		    
		   }
}


