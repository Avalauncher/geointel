package com.tecsq.geointel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.tecsq.filechecker.FileChecker;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;

public class GeoIntelStarter extends Service {
	
	private Date devDate, runDate, stopDate;
	public static final String inputFormat = "h:mm a";
	@SuppressLint("SimpleDateFormat")
	SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

    public GeoIntelStarter() {}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
    @SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		String curr_date = df.format(Calendar.getInstance().getTime());
		String run_Date = "8:00 AM";
		String stop_Date = "6:00 PM";
		runDate  = parseDate(run_Date);
		devDate = parseDate(curr_date);
		stopDate = parseDate(stop_Date);
		
		if(devDate.after(runDate) && devDate.before(stopDate) ) {
			System.out.println("GeoIntelStarter-HEY WORK NOW!!!");
			
			if(!FileChecker.isMyServiceRunning(ServiceMain.class, GeoIntelStarter.this)) {
				startSAppServices();
			}

		}

    }
    
    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
    
	public void startSAppServices() {
		
    	try {
    		
    		Intent i = new Intent(Intent.ACTION_MAIN);
	    	i.setComponent(new ComponentName("com.tecsq.geointel","com.tecsq.geointel.Geo_Main"));
	        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        this.startActivity(i);
	        
	        startService(new Intent(this, ServiceMain.class));

    	    stopSelf();

    	} catch(Exception e) {
    		e.printStackTrace();
    	}			
		
	}
    
}


